/// <reference types="cypress" />

const { WSASYSCALLFAILURE } = require("constants")
const { watchFile } = require("fs")
const { waitForDebugger } = require("inspector")

describe('Signup',function(){
it('Signs up Student successfully and checks for presence of correct elements',function(){
   // access url (with Instructor invite) https://dev.goreact.com/join/62acef0a-f743-4816-a1a4-be9657cd87e9
    cy.visit('https://dev.goreact.com/join/62acef0a-f743-4816-a1a4-be9657cd87e9')

    // checking presence of buttons and clicking buttons to sign up
    cy.get('[data-cy=create-account]').contains('Create Account').should('be.visible').click()
    cy.get('[data-cy=course-confirm-continue]').contains('Continue').should('be.visible').click()

    // Registration process fill-up necessary data and checking all fields
    cy.get('#first-name').type('STestFirstName')
    cy.get('#last-name').type('STestLastName')
    cy.get('#email').type('NewStudentEmail110@gmail.com')
    cy.get('#password').type('Abcd1234!')
    cy.get('#confirm-password').type('Abcd1234!')
    cy.get('#terms').check()
    cy.get('[data-cy=signup-form-submit]').contains('Continue').should('be.visible').click()


    //checking of terms and condition link and accepting terms
    cy.get('a.ng-scope').eq(2).click({force: true})
    cy.get('a.ng-scope').eq(3).click({force: true})
    cy.get('#accept-terms').check()

    //clicking registration buttons
    cy.get('button.primary-btn.pull-right.ng-scope').contains('Continue').should('be.visible').click({force: true})

    //Checking for a sample element to check if student successfully registered and now on Homepage
    cy.get('div.course-name.ng-binding').contains('Test Course').should('be.visible')

    //calls logout() function (see cypress/support/commands.js) to logout user
    cy.logout()

    //checking a sample element if the student successfully logged out and now on login page
    cy.get('[data-cy=login-button]').contains('Log In').should('be.visible')
})
})