/// <reference types="cypress" />

describe('signup',function(){
it('Checks if student will be denied if no Course Invitation',function(){
    // access url https://dev.goreact.com/dashboard/auth/login
    cy.visit('https://dev.goreact.com/dashboard/auth/login')


    // checking presence of buttons and clicking buttons to sign up
    cy.get('[data-cy=create-account]').contains('Create Account').should('be.visible').click()
    cy.get('[data-cy=us-confirm-button]').contains('No').should('be.visible').click()
    cy.get('[data-cy=stud-sign-up-button]').contains('STUDENT').should('be.visible').click()

    //called CheckAccessingYourCourseText() function (see cypress/support/commands.js)
    //Check if Text if correct in Accessing Your Course Page then click 'Back'
    cy.CheckAccessingYourCourseText()
    cy.get('button.primary-btn.ng-scope').contains('Back').should('be.visible').click()

    //checking a sample element if the student successfully logged out and now on login page
    cy.get('button.secondary-btn.ng-scope').contains('Create Account').should('be.visible')
})
})