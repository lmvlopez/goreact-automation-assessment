/// <reference types="cypress" />

describe('Login',()=>{
it('Checks if Error Message appears if user logs in with incorrect credentials',()=>{
    
    // access url https://dev.goreact.com/dashboard/auth/login
    cy.visit('https://dev.goreact.com/dashboard/auth/login')

    // fill-up email and password fields
    cy.get('#email').type('StudentTest17@gmail.com')
    cy.get('#password').type('123')

    //press login button
    cy.get('button.primary-btn.submit-btn').should('be.visible').click()

    //Check if Check username and password is will appear
    cy.get('p.alert-message.ng-scope').contains('Check username and password').should('be.visible')
})
})