/// <reference types="cypress" />

describe('Axe Accessibility Test',()=>{
it('should test basic Accessibility issues on Instructor Signup page',()=>{

    // access url https://dev.goreact.com/dashboard/auth/login
    cy.visit('https://dev.goreact.com/dashboard/auth/signup')

    //navigate to instructor signup page
    cy.get('button.primary-btn.signup-btn.btn-block.ng-scope').contains('INSTRUCTOR').click()

    //use Cypress-axe to automate accessibility testing
    cy.injectAxe();
    cy.checkA11y('div.content-container');   
})
})