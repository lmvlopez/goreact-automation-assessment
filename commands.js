Cypress.Commands.add('logout',()=>{
    cy.wait(5)
    cy.get('#user-navigation-dropdown-btn').should('be.visible').click()
    cy.get('[data-cy=logout]').should('be.visible').click({force:true})
})

Cypress.Commands.add('login',(email,password)=>{
    cy.wait(5)
    cy.get('#email').type(email)
    cy.get('#password').type(password)
    cy.get('button.primary-btn.submit-btn').should('be.visible').click()
})

Cypress.Commands.add('CheckAccessingYourCourseText',()=>{
    cy.get('p.ng-scope').contains("In order to sign up as a student in GoReact, you'll need a course invitation or registration link from your instructor. Please reach out to your instructor and request that they invite you to their GoReact course.")
})
