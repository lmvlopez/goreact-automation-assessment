/// <reference types="cypress" />

describe('Login',function(){
it('Logs in Instructor Successfully and checks for presence of correct elements',function(){
    // access url https://dev.goreact.com/dashboard/auth/login
    cy.visit('https://dev.goreact.com/dashboard/auth/login')

    // fill-up email and password fields
    cy.get('#email').type('fakeemail1@gmail.com')
    cy.get('#password').type('Abcd1234!')

    //press login button
    cy.get('[data-cy=login-button]').contains('Log In').should('be.visible').click()

    //Checking for a sample element to check if the instructor successfully registered and now on Homepage
    cy.get('button.link-btn.instructions-popup-trigger.ng-binding').contains('VIEW INSTRUCTIONS').should('be.visible')

    //calls logout() function (see cypress/support/commands.js) to logout user
    cy.logout()

    //checking a sample element to check if the instructor successfully logged out and now on login page
    cy.get('button.link-btn.ng-scope').contains('FORGOT PASSWORD').should('be.visible')
})
})