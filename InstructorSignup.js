/// <reference types="cypress" />

describe('Signup',function(){
it('Signs up Instructor successfully and checks for presence of correct elements',function(){

    // access url https://dev.goreact.com/dashboard/auth/login
    cy.visit('https://dev.goreact.com/dashboard/auth/login')

    // checking presence of buttons and clicking buttons to sign up
    cy.get('[data-cy=create-account]').contains('Create Account').should('be.visible').click()
    cy.get('[data-cy=us-confirm-button]').contains('No').should('be.visible').click()
    cy.get('[data-cy=ins-sign-up-button]').contains('INSTRUCTOR').should('be.visible').click()

    // Registration process fill-up necessary data and checking all fields
    cy.get('#first-name').type('Ifirstname')
    cy.get('#last-name').type('Ilastname')
    cy.get('#phone-number').type('+631234567890')
    cy.get('#email').type('enternewInstructorEmail9@gmail.com')
    cy.get('#password').type('Abcd1234!')
    cy.get('#confirm-password').type('Abcd1234!')
    cy.get('#terms').click()
    cy.get('[data-cy=signup-form-submit]').contains('Continue').should('be.visible').click()
    cy.get('#org_type_selector').click()
    cy.contains('Higher Education Institution').click({force:true})
    cy.get('div.form-group.typeahead-input-group').type('My Univer')
    cy.contains('My University').click({force:true})
    cy.get('#roles_selector').click()
    cy.contains('Dean').click({force:true})
    cy.get('#course_type_selector').click()
    cy.contains('Research').click({force:true})
    cy.get('#course_format').click()
    cy.contains('Online').click({force:true})
    cy.get('[data-cy=acct-setup-submit]').contains('Continue').should('be.visible').click()

    //Checking for a sample element to check if the instructor successfully registered and now on Homepage
    cy.get('[data-cy=course-menu-toggle]').contains("Courses").should('be.visible')

    //calls logout() function (see cypress/support/commands.js) to logout user
    cy.logout()

    //checking a sample element to check if the instructor successfully logged out and now on login page
    cy.get('[data-cy=create-account]').contains('Create Account').should('be.visible')
})
})