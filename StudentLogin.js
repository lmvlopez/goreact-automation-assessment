/// <reference types="cypress" />

describe('Login',()=>{
it('Logs in Student successfully and checks for presence of correct elements',()=>{

    // access url https://dev.goreact.com/dashboard/auth/login
    cy.visit('https://dev.goreact.com/dashboard/auth/login')

    // fill-up email and password fields
    cy.get('#email').type('StudentTest17@gmail.com')
    cy.get('#password').type('Abcd1234!')

    //press login button
    cy.get('button.primary-btn.submit-btn').should('be.visible').click()

    //Checking for a sample element to check if the student successfully registered and now on Homepage
    cy.get('button.btn.primary-btn.pay-btn.card.ng-scope').contains('Pay with Card').should('be.visible')

    //calls logout() function (see cypress/support/commands.js) to logout user
    cy.logout()

    //checking a sample element if the student successfully logged out and now on login page
    cy.get('#email').should('be.visible')
})
})