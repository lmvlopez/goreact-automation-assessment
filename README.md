# GoReact Automation Assessment

Cypress Automation Assessment for GoReact


# Installation:
1.) Install Code Editor:VS code (Microsoft)

2.) Install Node.js and NPM

3.) Cypress

4.) On package.json type "npm run cypress"


# Positive Test and Negative

1.) Positive testing and Successful login are combined on the same file

2.) For Student Sign-up before running the code make sure to input new email address
`cy.get('#email').type('enternewInstructorEmail@gmail.com')` and `cy.get('#email').type('StudentTest35@gmail.com')`

3.) No special instruction on running the code or save the .js file to run test

# Accessibility Testing

1.) Install cypress-axe

    * On your created cypress folder, run cmd and type 
    `npm install --save-dev cypress-axe`
    then
    `npm install --save-dev cypress axe-core`

    *Include the commands. Update cypress/support/index.js file to include the cypress-axe commands by adding:

    `import 'cypress-axe'`

    2.) Cypress-axe is used for automating Accessibilityt Testing
