/// <reference types="cypress" />

describe('Signup',function(){
it('Checks if error message apeears if Terms and Privacy is not checked',function(){
// access url (with Instructor invite) https://dev.goreact.com/join/62acef0a-f743-4816-a1a4-be9657cd87e9
cy.visit('https://dev.goreact.com/join/62acef0a-f743-4816-a1a4-be9657cd87e9')

// checking presence of buttons and clicking buttons to sign up
cy.get('[data-cy=create-account]').contains('Create Account').should('be.visible').click()
cy.get('[data-cy=course-confirm-continue]').contains('Continue').should('be.visible').click()

// Registration process fill-up necessary data and checking all fields
cy.get('#first-name').type('STestFirstName')
cy.get('#last-name').type('STestLastName')
cy.get('#email').type('StudentTest34@gmail.com')
cy.get('#password').type('Abcd1234!')
cy.get('#confirm-password').type('Abcd1234!')

//Uncheck terms and Privacy Policy and click 'Continue'
cy.get('#terms').uncheck()
cy.get('[data-cy=signup-form-submit]').contains('Continue').should('be.visible').click()

//Check if error message Please accept terms and privacy policy. appears
cy.get('span.ng-scope').contains('Please accept terms and privacy policy.').should('be.visible')
})
})